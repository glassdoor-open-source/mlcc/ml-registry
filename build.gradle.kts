val artifactoryBaseUrl = "<ARTIFACTORY-BASE-URL>"
val artifactoryUrl = artifactoryBaseUrl + "<ARTIFACTORY-URL"
val envRepoBaseUrl: String? = project.properties["your_maven_base_url"] as String?
val repoBaseUrl = if (!envRepoBaseUrl.isNullOrEmpty()) envRepoBaseUrl else artifactoryBaseUrl
val artifactoryReleaseUsername: String? by project
val artifactoryReleasePassword: String? by project

plugins {
    id("java")
    id("maven-publish")
    id("com.adarshr.test-logger") version "3.2.0"
    id("com.github.ben-manes.versions") version "0.42.0"
    id("com.diffplug.spotless") version "6.4.2"
    id("io.freefair.lombok") version "6.4.2"
    id("org.springframework.boot") version "2.6.6" apply(false)
    id("org.openapi.generator") version "6.2.1" apply(false)
}

java {
    // Generate source jar to publish
    withSourcesJar()

    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}

configure<com.diffplug.gradle.spotless.SpotlessExtension> {
    java {
        importOrder()
        removeUnusedImports()
        googleJavaFormat("1.15.0").reflowLongStrings()
        targetExclude("**/generated/**")
    }
    kotlinGradle {
        ktlint("0.45.1")
    }
}

allprojects {
    apply(plugin = "java")
    apply(plugin = "com.adarshr.test-logger")
    apply(plugin = "com.github.ben-manes.versions")
    apply(plugin = "com.diffplug.spotless")
    apply(plugin = "io.freefair.lombok")
    apply(plugin = "maven-publish")

    repositories {
        mavenCentral()
        maven {
            url = uri(artifactoryUrl)
        }
    }

    configurations.all {
        resolutionStrategy {
            preferProjectModules()
        }
    }

    dependencies {
        // Gradle Platforms for Dependency Management

        // Spring
        annotationProcessor(platform(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES))
        implementation(platform(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES))

        // AWS
        implementation(platform("com.amazonaws:aws-java-sdk-bom:1.12.211"))

        // JUnit
        testImplementation("org.mockito:mockito-core:2.1.0")

        // SnakeYaml
        implementation("org.yaml:snakeyaml:1.30")

        // GitLab
        implementation("org.gitlab4j:gitlab4j-api:5.0.1")

        // Redis Redisson Client
        implementation("org.redisson:redisson:3.17.7")
    }

    tasks.withType<Test> {
        useJUnitPlatform()
        if (environment["SERVICE_NAME"] == null) {
            environment(mapOf("SERVICE_NAME" to "ml-registry-app"))
        }
    }

    publishing {
        repositories {
            maven {
                url = uri("$repoBaseUrl/releases")

                credentials {
                    username = artifactoryReleaseUsername
                    password = artifactoryReleasePassword
                }
            }
        }
    }
}
