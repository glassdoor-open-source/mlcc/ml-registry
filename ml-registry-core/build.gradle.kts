plugins {
}

dependencies {

    implementation(project(":ml-registry-api"))

    // Spring
    implementation("org.springframework:spring-web")
    implementation("org.springframework:spring-context")

    // AWS
    implementation("com.amazonaws:aws-java-sdk-s3")
    implementation("com.amazonaws:aws-java-sdk-sqs")

    // Logging
    implementation("org.slf4j:slf4j-api")

    // Redisson
    implementation("org.redisson:redisson")

}