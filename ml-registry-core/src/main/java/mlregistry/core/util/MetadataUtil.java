/*
 *
 * Copyright 2022-2023 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * MetadataUtil.java
 *
 *
 */

package mlregistry.core.util;

import mlregistry.core.model.ProjectArtifactMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.LinkedHashMap;
import java.util.Map;

@Component
public class MetadataUtil {

    @Autowired
    public MetadataUtil(){
    }

    public String generateCacheKey(String projectName, String variant){
        return projectName + "-" + variant;
    }

    public ProjectArtifactMetadata updateProjectMetadata(ProjectArtifactMetadata projectArtifactMetadata, String artifactName, String dataObjectId){
        Map<String, LinkedHashMap> projectMetatadataMap = projectArtifactMetadata.getArtifactMetadataMap();

        // Get the contents of the specific artifact from artifacts.yaml
        LinkedHashMap artifact = projectMetatadataMap.get(artifactName);

        // Update the dataObjectId for this artifact
        // TODO Let's allow for us to update more than just the dataObjectId. And we can probably put all of these updates in a helper method
        artifact.put("dataObjectId", dataObjectId);

        return new ProjectArtifactMetadata(projectMetatadataMap);
    }
}
