/*
 *
 * Copyright 2022-2023 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * RedisUtil.java
 *
 *
 */

package mlregistry.core.util;

import mlregistry.core.model.ProjectArtifactMetadata;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.InternalServerErrorException;

@Component
public class RedisUtil {

    private MetadataUtil metadataUtil;
    private RedissonClient redissonClient;
    Logger logger = LoggerFactory.getLogger(RedisUtil.class);


    @Autowired
    public RedisUtil(MetadataUtil metadataUtil, RedissonClient redissonClient){
        this.metadataUtil = metadataUtil;
        this.redissonClient = redissonClient;
    }

    public ProjectArtifactMetadata getProjectMetadataFromRedis(String projectName, String variant){

        logger.info("Getting ProjectMetadata from Redis for ProjectName: {}, Variant: {}", projectName, variant);
        String bucketName = metadataUtil.generateCacheKey(projectName, variant);

        try {
            RBucket<ProjectArtifactMetadata> projectMetadataBucket = redissonClient.getBucket(bucketName);
            return projectMetadataBucket.get();
        } catch (Exception e){
            logger.error("Unable to retrieve Bucket from Redis for ProjectName: {}, Variant: {}", projectName, variant);
            throw new InternalServerErrorException();
        }
    }
}
