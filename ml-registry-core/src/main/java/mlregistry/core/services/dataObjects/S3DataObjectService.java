/*
 *
 * Copyright 2022-2023 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * S3DataObjectService.java
 *
 *
 */

package mlregistry.core.services.dataObjects;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.Upload;
import com.amazonaws.services.s3.transfer.model.UploadResult;
import mlregistry.api.model.response.DataObjectResponse;
import mlregistry.core.config.properties.S3ClientProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import java.io.IOException;
import java.io.InputStream;

import static org.apache.commons.codec.digest.DigestUtils.md5Hex;

@Service
public class S3DataObjectService implements DataObjectService<MultipartFile, S3Object> {

    private TransferManager transferManager;
    private AmazonS3 s3Client;
    private S3ClientProperties s3ClientProperties;

    Logger logger = LoggerFactory.getLogger(S3DataObjectService.class);

    @Autowired
    public S3DataObjectService(TransferManager transferManager, AmazonS3 s3Client, S3ClientProperties s3ClientProperties){
        this.transferManager = transferManager;
        this.s3Client = s3Client;
        this.s3ClientProperties = s3ClientProperties;
    }

    /**
     * Schedules a new transfer to upload data to Amazon S3. This method is non-blocking and returns immediately
     * (i.e. before the upload has finished). However, for our use case, we will wait to confirm the completion of the file
     * upload by calling waitForUploadResult().
     *
     * When uploading options from a stream, callers MUST supply the size of options in the stream through the content length
     * field in the ObjectMetadata parameter. If no content length is specified for the input stream, then TransferManager
     * will attempt to buffer all the stream contents in memory and upload the options as a traditional, single part upload.
     * Because the entire stream contents must be buffered in memory, this is expensive and not feasible for large files,
     * and should be avoided whenever possible.
     *
     * We can also use the returned Upload object to query the progress of the transfer, add listeners for progress
     * events, and wait for the upload to complete.
     *
     * @param dataObject - Spring Boot MultipartFile will never be held in memory. File contents will first be written
     *                   to disk, where we can then stream the file to S3 without memory concerns.
     * @return - A DataObjectResponse, the representation of the dataObject which we will return to the client.
     */
    @Override
    public DataObjectResponse uploadDataObject(MultipartFile dataObject){
        logger.info(
                "Starting S3 Upload Request for file with OriginalFileName: {}, Size: {}, ContentType: {}",
                dataObject.getOriginalFilename(),
                dataObject.getSize(),
                dataObject.getContentType()
        );

        /**
         * Assigning the contentLength attribute is mandatory. Without it, the TransferManager will attempt to load the
         * entire file in memory.
         */
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(dataObject.getSize());

        try {
            InputStream md5InputStream = dataObject.getInputStream();
            InputStream s3InputStream = dataObject.getInputStream();

            //Create a unique identifier for this file a stream. The entire contents of file are never stored in memory.
            String dataObjectId = md5Hex(md5InputStream);
            md5InputStream.close();

            Upload upload = transferManager.upload(
                    s3ClientProperties.getS3BucketName(),
                    dataObjectId,
                    s3InputStream,
                    metadata
            );

            UploadResult uploadResult = upload.waitForUploadResult();
            s3InputStream.close();

            logger.info("Upload Request Complete, UploadResult Key: {}, Bucket: {}", uploadResult.getKey(), uploadResult.getBucketName());
            return new DataObjectResponse(
                    uploadResult.getKey()
            );
        } catch (AmazonServiceException ex) {
            logger.error("Encountered AmazonServiceException while uploading Object to S3: {}, DataObjectName: {}",
                    ex.getMessage(), dataObject.getName());
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Encountered AmazonServiceException while uploading Object to S3",
                    ex
            );
        } catch(IOException ex){
            logger.error("Encountered IOException while uploading Object to S3: Error: {}", ex.getMessage());
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Encountered IOException while uploading Object to S3",
                    ex
            );
        } catch(Exception ex){
            logger.error("Error: {}", ex.getMessage());
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Exception while uploading Object to S3",
                    ex
            );

        }
    }

    /**
     * Retrieves objects from Amazon S3.
     *
     * @param dataObjectId String identifier of the object to retrieve from S3
     * @return S3Object - Represents an object stored in Amazon S3. This object contains the data content and the
     * object metadata stored by Amazon S3, such as content type, content length, etc
     */
    @Override
    public S3Object fetchDataObject(String dataObjectId) {
        logger.info("Starting S3 GetObject Request for DataObjectId: {}", dataObjectId);

        try {
            return s3Client.getObject(s3ClientProperties.getS3BucketName(), dataObjectId);
        } catch(SdkClientException ex){
            //Catch Client Exception
            logger.error("Error downloading data object {} from S3: {}, DataObjectId: {}", dataObjectId, ex.getMessage());
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "SdkClientException encountered while downloading data object from S3",
                    ex
            );
        } catch(Exception ex){
            logger.error("Error: {}", ex.getMessage());
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    "Error downloading data object from S3",
                    ex
            );
        }
    }
}
