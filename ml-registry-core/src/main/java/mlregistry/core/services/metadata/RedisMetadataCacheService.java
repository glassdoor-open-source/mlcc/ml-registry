/*
 *
 * Copyright 2022-2023 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * RedisMetadataCacheService.java
 *
 *
 */

package mlregistry.core.services.metadata;

import lombok.Data;
import mlregistry.api.model.request.ArtifactUpdateRequest;
import mlregistry.core.config.properties.MlRegistryProperties;
import mlregistry.core.model.ProjectArtifactMetadata;
import mlregistry.core.util.MetadataUtil;
import mlregistry.core.util.RedisUtil;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.Yaml;

import javax.ws.rs.InternalServerErrorException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

@Service("redisMetadataCacheService")
@Data
public class RedisMetadataCacheService implements MetadataCacheService{

    private RedissonClient redissonClient;
    private MetadataUtil metadataUtil;
    private Yaml snakeYamlService;
    private RedisUtil redisUtil;
    private MlRegistryProperties mlRegistryProperties;

    Logger logger = LoggerFactory.getLogger(RedisMetadataCacheService.class);


    @Autowired
    public RedisMetadataCacheService(RedissonClient redissonClient,
                                MetadataUtil metadataUtil,
                                Yaml snakeYamlService,
                                RedisUtil redisUtil,
                                MlRegistryProperties mlRegistryProperties){
        this.redissonClient = redissonClient;
        this.snakeYamlService = snakeYamlService;
        this.metadataUtil = metadataUtil;
        this.redisUtil = redisUtil;
        this.mlRegistryProperties = mlRegistryProperties;
    }

    @Override
    public void postAllMetadata(Map<String, ProjectArtifactMetadata> metadata){
        logger.info("Attempting to post all artifact metadata to Redis");
        for (Map.Entry<String, ProjectArtifactMetadata> entry : metadata.entrySet()){
            postArtifactMetadata(entry.getKey(), entry.getValue());
        }
    }

    /**
     * Update the project artifact metadata in redis
     * @param updateRequest
     */
    @Override
    public void updateArtifactMetadata(ArtifactUpdateRequest updateRequest) {

        logger.info("Updating ProjectMetadata in redis for project: {} and variant: {}",
                updateRequest.getProjectName(), updateRequest.getVariant());


        String bucketName = metadataUtil.generateCacheKey(updateRequest.getProjectName(), updateRequest.getVariant());

        try {
            RBucket<ProjectArtifactMetadata> projectBucket = redissonClient.getBucket(bucketName);

            if(Objects.nonNull(projectBucket.get())) {
                ProjectArtifactMetadata updatedMetadata = metadataUtil.updateProjectMetadata(
                        projectBucket.get(),
                        updateRequest.getArtifactName(),
                        updateRequest.getDataObjectId()
                );

                projectBucket.set(updatedMetadata);
            } else{
                logger.warn("Unable to update non-existent object. ProjectName: {}, Variant:{}",
                        updateRequest.getProjectName(), updateRequest.getVariant());
            }
        } catch(Exception e){
            logger.error("Failed to update project Metadata in Redis. ProjectName: {}, Variant: {}",
                    updateRequest.getProjectName(), updateRequest.getVariant());

            throw new InternalServerErrorException("Failed to update project Metadata in Redis");
        }

    }

    @Override
    public Map getArtifactMetadata(String projectName, String variant, String artifactName) {

        logger.info("Getting Artifact Metadata from Redis ProjectName: {}, Variant: {}, ArtifactName: {}",
                projectName, variant, artifactName);

        ProjectArtifactMetadata projectArtifactMetadata = redisUtil.getProjectMetadataFromRedis(projectName, variant);
        Map<String, LinkedHashMap> projectMetatadataMap = projectArtifactMetadata.getArtifactMetadataMap();
        LinkedHashMap artifact = projectMetatadataMap.get(artifactName);

        logger.info("Successfully loaded Artifact Metadata from Redis ProjectName: {}, Variant: {}, ArtifactName: {}",
                projectName, variant, artifactName);

        return artifact;
    }

    @Override
    public ProjectArtifactMetadata getProjectArtifactMetadata(String projectName, String variant){
        return redisUtil.getProjectMetadataFromRedis(projectName, variant);
    }

    public void postArtifactMetadata(String bucketName, ProjectArtifactMetadata artifactMetadata){
        RBucket<ProjectArtifactMetadata> projectMetadataBucket = redissonClient.getBucket(bucketName);
        projectMetadataBucket.set(artifactMetadata);
    }
}
