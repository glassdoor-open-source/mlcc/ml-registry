/*
 *
 * Copyright 2022-2023 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * GitLabMetadataService.java
 *
 *
 */

package mlregistry.core.services.metadata;

import com.amazonaws.util.StringUtils;
import lombok.Data;
import mlregistry.api.model.request.ArtifactUpdateRequest;
import mlregistry.core.config.properties.MlRegistryProperties;
import mlregistry.core.model.ProjectArtifactMetadata;
import mlregistry.core.model.GitLabUpdateRequest;
import mlregistry.core.util.MetadataUtil;
import org.gitlab4j.api.GitLabApi;
import org.gitlab4j.api.models.Branch;
import org.gitlab4j.api.models.RepositoryFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.Yaml;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.InternalServerErrorException;
import java.util.*;;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import static mlregistry.core.constants.MlRegistryConstants.VARIANT_PREFIX;

@Service("gitlabMetadataService")
@Data
public class GitLabMetadataService implements MetadataService{

    Logger logger = LoggerFactory.getLogger(GitLabMetadataService.class);

    private Yaml snakeYamlService;
    private GitLabApi gitLabApi;
    private MlRegistryProperties mlRegistryProperties;
    private MetadataCacheService metadataCacheService;
    private MetadataUtil metadataUtil;
    private static final String ARTIFACTS_YML = "artifacts.yml";
    private Map<String, Integer> gitlabProjectNameToIdMap = new HashMap<>();

    @Autowired
    public GitLabMetadataService(GitLabApi gitLabApi,
                                 Yaml snakeYamlService,
                                 MlRegistryProperties mlRegistryProperties,
                                 MetadataCacheService metadataCacheService,
                                 MetadataUtil metadataUtil){
        this.gitLabApi = gitLabApi;
        this.snakeYamlService = snakeYamlService;
        this.mlRegistryProperties = mlRegistryProperties;
        this.metadataCacheService = metadataCacheService;
        this.metadataUtil = metadataUtil;
    }

    /**
     * Write updated artifacts.yaml files to GitLab
     * @param gitLabUpdateRequest
     * @return boolean - Success/Failure
     */
    public boolean writeMetadataToRemote(GitLabUpdateRequest gitLabUpdateRequest) {

            if(Objects.isNull(gitLabUpdateRequest) ||
                    Objects.isNull(gitLabUpdateRequest.getVariant()) ||
                    Objects.isNull(gitLabUpdateRequest.getProjectName()) ||
                    Objects.isNull(gitLabUpdateRequest.getCommitMessage())){
                logger.error("Malformed UpdateRequest: {}", gitLabUpdateRequest.toString());
            }

            String artifactsYamlString = gitLabUpdateRequest.getArtifactsYaml();
            RepositoryFile artifactsYamlFile = new RepositoryFile();
            artifactsYamlFile.setContent(artifactsYamlString);
            artifactsYamlFile.setFileName(ARTIFACTS_YML);
            artifactsYamlFile.setFilePath(ARTIFACTS_YML);

            
            // Logging, error handling, exceptions handled in helper method.
            return writeRepositoryFile(
                        String.valueOf(gitlabProjectNameToIdMap.get(gitLabUpdateRequest.getProjectName())),
                        artifactsYamlFile,
                        gitLabUpdateRequest.getVariant(),
                        gitLabUpdateRequest.getCommitMessage()
            );

    }

    /**
     * Get all artifact metadata from GitLab
     * @return ConcurrentMap Key=Branch+Variant Value=ProjectArtifactMetadata (Java representation of artifacts.yaml)
     */
    @Scheduled(initialDelay = 21600000, fixedDelay = 21600000)
    public Map<String, ProjectArtifactMetadata> getAllMetadata() {
        //Make sure the projectNameToIdMap is up to date
        fetchProjectsFromRemote();

        logger.info("Attempting to retrieve artifact metadata from GitLab");

        ConcurrentMap<String, ProjectArtifactMetadata> allProjectMetadata = new ConcurrentHashMap<>();

        // For each repository, for all of its branches (variants), store the artifacts.yaml file in-memory
        // Exception handling for network requests delegated to helper methods
        if (Objects.nonNull(gitlabProjectNameToIdMap)) {
            for (String projectName : gitlabProjectNameToIdMap.keySet()) {
                List<Branch> branches = getBranches(projectName);
                for (Branch branch : branches) {
                    String variant = branch.getName();
                    RepositoryFile repositoryFile = getRepositoryFile(
                            projectName,
                            mlRegistryProperties.getArtifactsYaml(),
                            variant);
                    if (Objects.nonNull(repositoryFile)) {

                        // Create serializable java object from artifacts.yaml file from GitLab
                        Map<String, LinkedHashMap> artifactMap = snakeYamlService.load(repositoryFile.getDecodedContentAsString());
                        ProjectArtifactMetadata projectArtifactMetadata = new ProjectArtifactMetadata(artifactMap);


                        // Add this artifact metadata map to the overall map which will be shared with our cache
                        allProjectMetadata.put(metadataUtil.generateCacheKey(projectName, variant), projectArtifactMetadata);
                    }
                }
            }
        }

        return allProjectMetadata;
    }

    /**
     * Update artifacts.yaml configuration for a specified repository and branch
     * @param artifactRequest
     * @return
     */
    @Override
    public void updateArtifactMetadata(ArtifactUpdateRequest artifactRequest) {

        logger.info("Attempting to update artifacts.yaml for projectName: {}, variant: {}, and artifact: {}",
                artifactRequest.getProjectName(),
                artifactRequest.getVariant(),
                artifactRequest.getArtifactName());

        if(Objects.isNull(artifactRequest)
                || StringUtils.isNullOrEmpty(artifactRequest.getProjectName())
                || StringUtils.isNullOrEmpty(artifactRequest.getVariant())
                || StringUtils.isNullOrEmpty(artifactRequest.getCommitMessage())
                || StringUtils.isNullOrEmpty(artifactRequest.getDataObjectId())
                || StringUtils.isNullOrEmpty(artifactRequest.getArtifactName()))
            throw new BadRequestException();

        String projectName = artifactRequest.getProjectName();
        String variant = artifactRequest.getVariant();

        ProjectArtifactMetadata projectArtifactMetadata = metadataCacheService.getProjectArtifactMetadata(projectName, variant);

        if(Objects.nonNull(projectArtifactMetadata)){

            ProjectArtifactMetadata updatedProjectMetadata = metadataUtil.updateProjectMetadata(
                    projectArtifactMetadata,
                    artifactRequest.getArtifactName(),
                    artifactRequest.getDataObjectId()
            );

            try {
                // Attempt to post the update to GitLab
                Map<String, LinkedHashMap> updatedProjectMetadataMap = updatedProjectMetadata.getArtifactMetadataMap();

                //TODO This SnakeYaml dump operation does not preserve comments. Update this functionality to preserve comments
                String artifactsYaml = snakeYamlService.dump(updatedProjectMetadataMap);
                writeMetadataToRemote(new GitLabUpdateRequest(artifactRequest.getProjectName(), artifactRequest.getVariant(), artifactRequest.getCommitMessage(), artifactsYaml));
                logger.info("Successfully updated artifacts.yaml for projectName: {}, variant: {}", artifactRequest.getProjectName(), artifactRequest.getVariant());
            } catch(Exception ex){

                logger.error("Unable to update artifacts for projectName: {}, variant: {}. Exception: {}",
                        artifactRequest.getProjectName(),
                        artifactRequest.getVariant(),
                        ex.getMessage()
                );

                throw new InternalServerErrorException("Unable to post updated artifacts.yaml to GitLab");
            }
        } else{
            logger.warn("Supplied yamlConfig is identical to current config.");
            throw new BadRequestException("Supplied yamlConfig is identical to current config.");
        }

    }

    @Override
    public Map getArtifactMetadata(String projectName, String variant, String artifactName) {
        RepositoryFile repositoryFile = getRepositoryFile(projectName, ARTIFACTS_YML, variant);

        if(repositoryFile != null && !StringUtils.isNullOrEmpty(repositoryFile.getDecodedContentAsString())) {

            String artifactsYaml = repositoryFile.getDecodedContentAsString();

            // Deserialize the artifacts.yaml String from memory to a Java Object
            Map<String, LinkedHashMap> artifactsMap = snakeYamlService.load(artifactsYaml);

            // Get the specific artifact object we are updating as a java object
            LinkedHashMap artifact = artifactsMap.get(artifactName);

            if(artifact != null){
                return artifact;
            }

            logger.error("Attempt to get artifact metadata failed because artifact does not exist for " +
                    "projectName: {} and variant: {}", projectName, variant);

            throw new BadRequestException("Artifact Does Not Exist");
        } else{
            logger.error("Invalid projectName and variant " +
                    "projectName: {} and variant: {}", projectName, variant);

            throw new BadRequestException("Invalid projectName and variant");
        }
    }

    @Override
    public ProjectArtifactMetadata getProjectArtifactMetadata(String projectName, String variant){
        RepositoryFile repositoryFile = getRepositoryFile(projectName, ARTIFACTS_YML, variant);

        if(repositoryFile != null && !StringUtils.isNullOrEmpty(repositoryFile.getDecodedContentAsString())) {

            String artifactsYaml = repositoryFile.getDecodedContentAsString();

            // Deserialize the artifacts.yaml String from memory to a Java Object
            Map<String, LinkedHashMap> artifactsMap = snakeYamlService.load(artifactsYaml);

            return new ProjectArtifactMetadata(artifactsMap);
        } else{
            throw new BadRequestException("Artifact does not exist");
        }
    }

    /**
     *
     * @param projectName
     * @return The list of branches for the given repository Id
     */
    public List<Branch> getBranches(String projectName) {

        String repositoryId = String.valueOf(gitlabProjectNameToIdMap.get(projectName));

        logger.info("Attempting to fetch branches from projectName: {}", projectName);

        //Get the branches for this project
        try {
            List<Branch> branches = gitLabApi.getRepositoryApi().getBranches(repositoryId);

            if(Objects.nonNull(branches)){
                for(Branch b : branches) {
                    logger.info("Successfully fetched branch: {} for projectName: {}",
                            b.getName(),
                            repositoryId);
                }
            }

           return branches.stream()
                    //The ML-Registry only interacts with branches with the prefix "variant"
                   .filter(branch -> !StringUtils.isNullOrEmpty(branch.getName()) && branch.getName().startsWith(VARIANT_PREFIX))
                   .collect(Collectors.toList());

        } catch (Exception e) {
            logger.error("Failed to fetch branches from projectName: {}, Error: {}",
                    projectName, e.getMessage());
            throw new InternalServerErrorException();
        }
    }

    @Override
    public List<String> getVariants(String projectName){
        List<Branch> branches = getBranches(projectName);
        return branches.stream()
                .map(branch -> branch.getName())
                .collect(Collectors.toList());
    }

    /**
     * The ML-Registry must maintain knowledge of all of it's consumers. The list of these repositories is maintained
     * in the repositories.yaml file in the ML-Registry project in Gitlab. The names and ids of these repositories are kept
     * in-memory so that they can be read/written to.
     * @return boolean - Success/Failure
     */
    public boolean fetchProjectsFromRemote() {

        // Retrieve the name and id of repositories which the ML-Registry has to keep track of.
        // Exception handling delegated to helper method
        RepositoryFile repositoriesYaml = getRepositoryFile(
                mlRegistryProperties.getRepositoriesProjectId(),
                mlRegistryProperties.getRepositoriesYaml(),
                mlRegistryProperties.getMasterBranchName()
        );

        try{
            if(Objects.nonNull(repositoriesYaml)) {
                // Add the name and id of these repositories to our in-memory cache.
                gitlabProjectNameToIdMap = snakeYamlService.load(repositoriesYaml.getDecodedContentAsString());
            }
        } catch(Exception e){
            logger.error("Unable to load repositories.yaml from the ml-registry. Error: {}", e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * Helper method to fetch a RepositoryFile object from Gitlab.
     * @param projectName
     * @param filePath
     * @param branch (variant)
     * @return RepositoryFile - Gitlab4j's representation of a file stored in Gitlab
     */
    public RepositoryFile getRepositoryFile(String projectName, String filePath, String branch){

        /**
         * ** Special Use Case **
         *
         * When the app starts, the gitlabProjectNameToIdMap has not yet been populated. We need to call getRepositoryFile (this method)
         * to get the contents of this map on startup. In this instance, we directly pass the repositoryId to the getRepositoryFile method
         * (because we have no map to convert projectName to repositoryId yet).
         */
        String repositoryId = gitlabProjectNameToIdMap.isEmpty() ?
                projectName : String.valueOf(gitlabProjectNameToIdMap.get(projectName));

        logger.info("Attempting to load file: {}, from project: {} and branch: {} from Gitlab.",
                filePath, projectName, branch);

        try{
            RepositoryFile repositoryFile = gitLabApi.getRepositoryFileApi().getFile(
                    repositoryId,
                    filePath,
                    branch
            );

            logger.info("Successfully loaded RepositoryFile: {}, for projectName: {}, filePath: {}, branch: {}",
                    repositoryFile.getFileName(), projectName, filePath, branch);

            return repositoryFile;

        } catch (Exception e){
            logger.warn("Unable to fetch RepositoryFile from projectName: {}, branch: {}. Error: {}",
                    projectName, branch, e.getMessage());
            return null;
        }
    }

    /**
     * Write or update a RepositoryFile in Gitlab
     * @param projectName
     * @param file
     * @param branchName
     * @param commitMessage
     * @return
     */
    public boolean writeRepositoryFile(String projectName, RepositoryFile file, String branchName, String commitMessage) {

        logger.info("Attempting to update file: {} projectName: {}, branch: {}, commitMessage: {}",
                file.getFileName(), projectName, branchName, commitMessage);

        //Write the repository file to GitLab
        try {
            gitLabApi.getRepositoryFileApi().updateFile(
                    projectName,
                    file,
                    branchName,
                    commitMessage
            );
            logger.info("Successfully updated file: {} projectName: {}, branch: {}, commitMessage: {}",
                    file.getFileName(), projectName, branchName, commitMessage);

            return true;

        } catch (Exception e) {
            logger.error("Failed to update file: {} in projectName: {}, branch: {}. Error: {}",
                    file.getFileName(), projectName, branchName, e.getMessage());
            return false;
        }

    }

}
