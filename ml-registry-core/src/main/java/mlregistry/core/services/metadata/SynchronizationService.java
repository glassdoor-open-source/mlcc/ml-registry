/*
 *
 * Copyright 2022-2023 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SynchronizationService.java
 *
 *
 */

package mlregistry.core.services.metadata;

import com.amazonaws.services.sqs.model.Message;
import lombok.Data;

import mlregistry.api.model.request.ArtifactUpdateRequest;
import mlregistry.core.model.ProjectArtifactMetadata;
import mlregistry.core.services.messages.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import java.util.*;


@Service("synchronizationService")
@Data
@DependsOn("gitlabMetadataService")
public class SynchronizationService {

    private MetadataCacheService metadataCacheService;
    private MetadataService gitlabMetadataService;
    private MessageService sqsMessageService;

    Logger logger = LoggerFactory.getLogger(SynchronizationService.class);

    @Autowired
    public SynchronizationService(MetadataCacheService metadataCacheService,
                                  MetadataService gitlabMetadataService,
                                  MessageService sqsMessageService){
        this.metadataCacheService = metadataCacheService;
        this.gitlabMetadataService = gitlabMetadataService;
        this.sqsMessageService = sqsMessageService;
        synchronizeMetadataSources();
    }

    public void synchronizeMetadataSources(){
        metadataCacheService.postAllMetadata(gitlabMetadataService.getAllMetadata());
    }

    public void updateArtifactMetadata(ArtifactUpdateRequest artifactUpdateRequest){
        gitlabMetadataService.updateArtifactMetadata(artifactUpdateRequest);
        metadataCacheService.updateArtifactMetadata(artifactUpdateRequest);
        sqsMessageService.sendMessage(artifactUpdateRequest);
    }

    public Map getArtifactMetadata(String projectName, String variant, String artifactName) {
        return metadataCacheService.getArtifactMetadata(projectName, variant, artifactName);
    }

    public ProjectArtifactMetadata getProjectArtifactMetadata(String projectName, String variant){
        return metadataCacheService.getProjectArtifactMetadata(projectName, variant);
    }

    public List<String> getVariants(String projectName) {
        return gitlabMetadataService.getVariants(projectName);
    }
}
