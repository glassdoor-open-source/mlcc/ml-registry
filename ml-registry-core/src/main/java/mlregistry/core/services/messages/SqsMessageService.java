/*
 *
 * Copyright 2022-2023 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SqsMessageService.java
 *
 *
 */

package mlregistry.core.services.messages;

import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import mlregistry.api.model.request.ArtifactUpdateRequest;
import mlregistry.core.config.properties.SqsProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SqsMessageService implements MessageService<ArtifactUpdateRequest> {

    private AmazonSQS sqs;
    private SqsProperties sqsProperties;
    Logger logger = LoggerFactory.getLogger(SqsMessageService.class);

    @Autowired
    public SqsMessageService(AmazonSQS sqs, SqsProperties sqsProperties){
        this.sqs = sqs;
        this.sqsProperties = sqsProperties;
    }

    public void sendMessage(ArtifactUpdateRequest updateRequest){

        SendMessageRequest messageRequest = new SendMessageRequest()
                .withQueueUrl(sqsProperties.getSqsUrl())
                .withMessageBody(updateRequest.toString());

        try {
            sqs.sendMessage(messageRequest);
        } catch(Exception e){
            logger.error("Unable to send message to SQS queue. Message: {}, Error: {}", updateRequest.toString(), e.getMessage());
        }

    }
}
