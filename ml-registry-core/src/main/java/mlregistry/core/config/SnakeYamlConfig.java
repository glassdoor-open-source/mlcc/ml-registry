/*
 *
 * Copyright 2022-2023 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SnakeYamlConfig.java
 *
 *
 */

package mlregistry.core.config;

import mlregistry.core.config.properties.SnakeYamlProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

@Configuration
public class SnakeYamlConfig {

    private SnakeYamlProperties snakeYamlProperties;

    @Autowired
    public SnakeYamlConfig(SnakeYamlProperties snakeYamlProperties){
        this.snakeYamlProperties = snakeYamlProperties;
    }

    @Bean
    public Yaml dumperOptions(){

        // Configure SnakeYaml Preferences
        DumperOptions dumperOptions = new DumperOptions();
        dumperOptions.setIndent(snakeYamlProperties.getIndent());
        dumperOptions.setPrettyFlow(snakeYamlProperties.isPrettyFlow());
        dumperOptions.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);

        // Return Yaml Object
        return new Yaml(dumperOptions);

    }
}
