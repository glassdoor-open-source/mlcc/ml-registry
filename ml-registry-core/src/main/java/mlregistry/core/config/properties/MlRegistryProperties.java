/*
 *
 * Copyright 2022-2023 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * MlRegistryProperties.java
 *
 *
 */

package mlregistry.core.config.properties;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class MlRegistryProperties {

    /**
     * This value corresponds to the unique project id of the repository which holds the repositories.yaml file.
     * The repositories.yaml file holds a list of the repositories which the ML-Registry must maintain state of.
     */
    @Value("${ml.registry.repositories.id}")
    private String repositoriesProjectId;

    @Value("${ml.registry.repositories.name}")
    private String projectName;

    @Value("${ml.registry.branch.master}")
    private String masterBranchName;

    @Value("${ml.registry.artifacts.yaml}")
    private String artifactsYaml;

    @Value("${ml.registry.repositories.yaml}")
    private String repositoriesYaml;

    @Value("${ml.registry.gitlab.url}")
    private String gitlabUrl;

    @Value("${ml.registry.gitlab.token}")
    private String gitlabGroupAccessToken;



}
