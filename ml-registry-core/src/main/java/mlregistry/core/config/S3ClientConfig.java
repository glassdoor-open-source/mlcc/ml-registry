/*
 *
 * Copyright 2022-2023 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * S3ClientConfig.java
 *
 *
 */

package mlregistry.core.config;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;

import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import mlregistry.core.config.properties.S3ClientProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class S3ClientConfig {

    private S3ClientProperties s3ClientProperties;

    @Autowired
    public S3ClientConfig(S3ClientProperties s3ClientProperties){
        this.s3ClientProperties = s3ClientProperties;
    }

    @Bean
    public AmazonS3 amazonS3(){
        return AmazonS3Client.builder()
                .withRegion(s3ClientProperties.getAwsRegion())
                .build();
    }

    @Bean
    public TransferManager transferManager(){
        return TransferManagerBuilder.standard()
                .withS3Client(
                        AmazonS3Client.builder()
                                .withRegion(s3ClientProperties.getAwsRegion())
                                .build()
                ).build();
    }
}
