plugins {
    id("org.openapi.generator")
}


dependencies {

    implementation("io.gsonfire:gson-fire:1.8.5")
    implementation("com.squareup.okhttp3:logging-interceptor:4.10.0")
    implementation("jakarta.annotation:jakarta.annotation-api:2.1.0")
    implementation("com.google.code.findbugs:jsr305:3.0.2")
    implementation("javax.annotation:javax.annotation-api:1.3.2")
    implementation("io.swagger:swagger-annotations:1.6.8")
    implementation("com.squareup.okhttp3:okhttp:4.10.0")
    implementation("com.google.code.gson:gson:2.10.1")

}

apply(plugin = "java-library")

publishing {
    publications {
        register("library", MavenPublication::class) {
            from(components["java"])
            artifactId = "ml-registry-client"
        }
    }
}

openApiGenerate {
    generatorName.set("java")
    inputSpec.set("$rootDir/ml-registry-client/src/main/resources/ml-registry-app.yaml")
    outputDir.set("$buildDir/generated/java")
    packageName.set("mlr_client")
    generateModelDocumentation.set(false)
    generateApiDocumentation.set(false)
    generateApiTests.set(false)
    generateModelTests.set(false)
}


tasks.clean{
    doFirst{
        delete("$buildDir")
    }
}

// Ensure the generated model classes are available on the classpath
sourceSets {
    val main by getting
    main.java.srcDir("${openApiGenerate.outputDir.get()}/src/main/java")
    main.resources.srcDir("$buildDir/src/resources")
}

tasks.withType<JavaCompile> {
    dependsOn(tasks.named("openApiGenerate"))
    options.isWarnings = false
    options.compilerArgs = listOf<String>()
}

tasks.processResources {
    dependsOn(":ml-registry-client:openApiGenerate")
}

