val publishedArtifactName = "ml-registry-app"

plugins {
    id("org.springframework.boot")
}

dependencies {
    //Inter-Project Dependencies
    implementation(project(":ml-registry-core"))
    implementation(project(":ml-registry-api"))

    //Spring Boot
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    testImplementation("org.springframework.boot:spring-boot-starter-test")

    //AWS
    implementation("com.amazonaws:aws-java-sdk-s3")

    //SpringDoc
    implementation("org.springdoc:springdoc-openapi-ui:1.6.7")

}

springBoot{
    buildInfo{
        properties{
            artifact = publishedArtifactName
            name = "ML-Registry-App"
        }
    }
}

publishing {
    publications {
        register<MavenPublication>("mavenJava") {
            artifactId = publishedArtifactName
            artifact(tasks.getByName("bootJar"))
        }
    }
}

