/*
 *
 * Copyright 2022-2023 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * GitLabMetadataServiceITests.java
 *
 *
 */

package mlregistry.app.test.integration;


import mlregistry.core.services.metadata.GitLabMetadataService;
import mlregistry.core.util.MetadataUtil;
import org.gitlab4j.api.models.Branch;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Objects;

import static mlregistry.app.test.constants.TestConstants.*;


@ExtendWith(SpringExtension.class)
@SpringBootTest
public class GitLabMetadataServiceITests {

    @Autowired
    GitLabMetadataService gitLabMetadataService;

    @Autowired
    MetadataUtil metadataUtil;

    @Test
    void test_gitLabMetadataService_getBranches(){
        List<Branch> branches = gitLabMetadataService.getBranches(ML_REGISTRY_REPO_NAME);
        Assertions.assertTrue(Objects.nonNull(branches) && !branches.isEmpty());
        for(Branch branch : branches){
            Assertions.assertTrue(branch.getName().startsWith(VARIANT_PREFIX));
        }
    }

    @Test
    void test_gitLabMetadataService_getProjectNameToIdMap(){
        Assertions.assertTrue(!gitLabMetadataService.getGitlabProjectNameToIdMap().isEmpty());
    }

    @Test
    void test_gitLabMetadataService_getRepositoryFile(){
        Assertions.assertNotNull(gitLabMetadataService.getRepositoryFile(ML_REGISTRY_REPO_NAME, ARTIFACTS_YML, MASTER_BRANCH));
    }

    @Test
    void test_gitLabMetadataService_getVariants(){
        List<String> variants = gitLabMetadataService.getVariants(ML_REGISTRY_REPO_NAME);
        Assertions.assertTrue(!variants.isEmpty());
        for(String variant : variants){
            Assertions.assertTrue(variant.startsWith(VARIANT_PREFIX));
        }
    }

    @Test
    void test_gitLabMetadataService_getProjectArtifactMetadata(){
        Assertions.assertNotNull(gitLabMetadataService.getProjectArtifactMetadata(ML_REGISTRY_REPO_NAME, TEST_VARIANT));
    }
}
