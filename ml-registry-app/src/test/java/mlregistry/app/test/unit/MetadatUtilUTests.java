/*
 *
 * Copyright 2022-2023 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * MetadatUtilUTests.java
 *
 *
 */

package mlregistry.app.test.unit;

import mlregistry.core.util.MetadataUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static mlregistry.app.test.constants.TestConstants.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class MetadatUtilUTests {

    @Autowired
    MetadataUtil metadataUtil;

    @Test
    void test_metadataUtil_generateKey(){
        Assertions.assertEquals(metadataUtil.generateCacheKey(ML_REGISTRY_REPO_NAME, TEST_VARIANT), "ml-registry-variant-test");
    }
}
