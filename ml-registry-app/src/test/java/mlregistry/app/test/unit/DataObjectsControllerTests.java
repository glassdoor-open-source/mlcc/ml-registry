/*
 *
 * Copyright 2022-2023 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * DataObjectsControllerTests.java
 *
 *
 */

package mlregistry.app.test.unit;

import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import com.amazonaws.util.StringInputStream;
import mlregistry.api.model.response.DataObjectResponse;
import mlregistry.app.controllers.DataObjectsController;
import mlregistry.core.services.dataObjects.S3DataObjectService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import java.io.InputStream;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(DataObjectsController.class)
public class DataObjectsControllerTests {

    @MockBean
    S3DataObjectService s3DataObjectService;

    @Autowired
    MockMvc mockMvc;

    public static final String MOCK_OBJECT_ID = "123";
    public static final String MOCK_FILE_NAME = "file.txt";
    public static final String MOCK_FILE_CONTENTS = "Hello, ML-Registry";
    public static final String MOCK_PARAMETER_NAME = "dataObject";

    @Test
    void givenMultipartFile_whenUploadSucceeds_thenReturnValidDataObjectResponse() throws Exception{

        MockMultipartFile file = new MockMultipartFile(
                MOCK_PARAMETER_NAME,
                MOCK_FILE_NAME,
                MediaType.TEXT_PLAIN_VALUE,
                MOCK_FILE_CONTENTS.getBytes()
        );

        DataObjectResponse dataObjectResponse = new DataObjectResponse(MOCK_OBJECT_ID);
        Mockito.when(s3DataObjectService.uploadDataObject(file)).thenReturn(dataObjectResponse);

        //Make sure a 200 is returned, with a DataObjectResponse containing the object id.
        mockMvc.perform(multipart("/data-objects").file(file))
                .andExpect(content().json("{\"dataObjectId\": \"123\"}"))
                .andExpect(status().isOk());

    }

    @Test
    void givenValidFile_whenDownloadSucceeds_thenObjectIsStreamed() throws Exception{

        S3Object s3Object = Mockito.mock(S3Object.class);
        InputStream is = new StringInputStream(MOCK_FILE_CONTENTS);
        S3ObjectInputStream s3ObjectInputStream = new S3ObjectInputStream(is, null);

        Mockito.when(s3DataObjectService.fetchDataObject(MOCK_OBJECT_ID)).thenReturn(s3Object);
        Mockito.when(s3Object.getObjectContent()).thenReturn(s3ObjectInputStream);
        Mockito.when(s3Object.getKey()).thenReturn(MOCK_FILE_NAME);

        //Should return 200 with stream content type
        mockMvc.perform(get("/data-objects/123"))
                .andExpect(content().contentType(MediaType.APPLICATION_OCTET_STREAM))
                .andExpect(status().isOk());


    }
}
