/*
 *
 * Copyright 2022-2023 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * VariantsController.java
 *
 *
 */

package mlregistry.app.controllers;

import com.amazonaws.util.StringUtils;
import io.swagger.v3.oas.annotations.tags.Tag;
import mlregistry.api.model.response.VariantResponse;
import mlregistry.core.services.metadata.SynchronizationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Tag(name = "Variants", description = "Each project may have multiple variants. Each variant has a yaml configuration specifying pipeline behavior, artifacts, and other related metadata.")
@RestController
public class VariantsController {

    private SynchronizationService synchronizationService;

    public VariantsController(SynchronizationService synchronizationService){
        this.synchronizationService = synchronizationService;
    }

    @PostMapping("/variants")
    public ResponseEntity<VariantResponse> createNewVariant(
            @RequestParam (required = false) String projectName,
            @RequestParam (required = false) String existingVariant,
            @RequestParam (required = false) String newVariant
    ){
        //Empty method stub, implementation coming in future MR
        return new ResponseEntity<>(new VariantResponse(null), HttpStatus.OK);
    }

    @GetMapping("/variants/{projectName}")
    public ResponseEntity<VariantResponse> getDefaultVariant(@PathVariable String projectName){

        try{
            if(StringUtils.isNullOrEmpty(projectName)){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }

            List<String> variants = synchronizationService.getVariants(projectName);
            return new ResponseEntity<>(new VariantResponse(variants), HttpStatus.OK);

        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }

    }

    @PostMapping("/variants/{projectName}/default/{defaultVariant}")
    public ResponseEntity<VariantResponse> setDefaultVariant(@PathVariable String projectName, @PathVariable String defaultVariant){
        //Empty method stub, implementation coming in future MR
        return new ResponseEntity<>(new VariantResponse(null), HttpStatus.OK);
    }

}
