/*
 *
 * Copyright 2022-2023 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * ArtifactsController.java
 *
 *
 */

package mlregistry.app.controllers;

import com.amazonaws.util.StringUtils;
import io.swagger.v3.oas.annotations.tags.Tag;
import mlregistry.api.model.request.ArtifactUpdateRequest;
import mlregistry.api.model.response.ArtifactResponse;
import mlregistry.api.model.response.ArtifactUpdateResponse;
import mlregistry.core.services.metadata.SynchronizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.InternalServerErrorException;
import java.util.Map;

@Tag(name = "Artifacts", description = "The collection of metadata describing a Data Object and it's relationship to a project.")
@RestController
@DependsOn("synchronizationService")
public class ArtifactsController {

    Logger logger = LoggerFactory.getLogger(ArtifactsController.class);
    private SynchronizationService synchronizationService;

    @Autowired
    public ArtifactsController(SynchronizationService synchronizationService){
        this.synchronizationService = synchronizationService;
    }

    @PostMapping(
            value = "/artifacts",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<ArtifactUpdateResponse> updateArtifacts(@RequestBody ArtifactUpdateRequest artifactUpdateRequest){
        try {
            synchronizationService.updateArtifactMetadata(artifactUpdateRequest);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (BadRequestException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Malformed Request");
        } catch (InternalServerErrorException e){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Unable to post updated artifacts.yml to GitLab");
        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Internal Server Error");
        }
    }

    @GetMapping("/artifacts")
    public ResponseEntity<ArtifactResponse> getArtifact(
            @RequestParam (required = true) String projectName,
            @RequestParam (required = true) String variant,
            @RequestParam (required = true) String artifactName
    ){
        try{
            if(StringUtils.isNullOrEmpty(projectName) || StringUtils.isNullOrEmpty(variant) || StringUtils.isNullOrEmpty(artifactName)){
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
            }

            Map artifactMetadata = synchronizationService.getArtifactMetadata(projectName, variant, artifactName);
            return new ResponseEntity<>(new ArtifactResponse(artifactName, artifactMetadata), HttpStatus.OK);

        } catch (Exception e){
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @PostMapping("/artifacts/refresh")
    public ResponseEntity<Boolean> refreshArtifactMetadata(){
        synchronizationService.synchronizeMetadataSources();
        return new ResponseEntity(HttpStatus.OK);
    }

}
