/*
 *
 * Copyright 2022-2023 Glassdoor, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * DataObjectsController.java
 *
 *
 */

package mlregistry.app.controllers;

import com.amazonaws.services.s3.model.S3Object;
import io.micrometer.core.instrument.util.StringUtils;
import io.swagger.v3.oas.annotations.tags.Tag;
import mlregistry.api.model.response.DataObjectResponse;
import mlregistry.core.services.dataObjects.DataObjectService;
import mlregistry.core.services.dataObjects.S3DataObjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.io.InputStream;
import java.util.Objects;

@Tag(name = "Data Objects", description = "The data objects in storage.")
@RestController
public class DataObjectsController {
    Logger logger = LoggerFactory.getLogger(DataObjectsController.class);
    private static final String CONTENT_DISPOSITION_INLINE = "inline";
    private final DataObjectService s3DataObjectService;

    @Autowired
    public DataObjectsController(S3DataObjectService s3DataObjectService){
        this.s3DataObjectService = s3DataObjectService;
    }

    @PostMapping(
            value = "/data-objects",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<DataObjectResponse> postDataObject(@RequestParam MultipartFile dataObject)  {

        DataObjectResponse dataObjectResponse = s3DataObjectService.uploadDataObject(dataObject);


        //Valid object identifier signals a successfull upload to S3
        if(Objects.nonNull(dataObjectResponse) && StringUtils.isNotEmpty(dataObjectResponse.getDataObjectId())){
            return ResponseEntity
                    .ok()
                    .body(dataObjectResponse);
        } else{
            return ResponseEntity
                    .badRequest()
                    .body(dataObjectResponse);
        }
    }

    @GetMapping(
            value = "/data-objects/{dataObjectId}",
            produces = MediaType.MULTIPART_FORM_DATA_VALUE
    )
    public ResponseEntity<Resource> getDataObject(@PathVariable String dataObjectId) {
        S3Object dataObject = (S3Object) s3DataObjectService.fetchDataObject(dataObjectId);

        if(dataObject != null) {
            logger.info("Successfully Downloaded DataObject Id: {}", dataObject.getKey());
            return streamResource(dataObject);
        }

        return ResponseEntity
                .badRequest()
                .build();
    }

    @RequestMapping(
            value ="/data-objects/{dataObjectId}",
            method = { RequestMethod.HEAD })
    public ResponseEntity checkDataObjectExists(@PathVariable String dataObjectId){
        //Empty method stub, implementation coming in future MR
        return new ResponseEntity(false, HttpStatus.OK);
    }

    /**
     * Streams the S3Object returned from S3 as a Resource.
     *
     * @param dataObject - S3Object returned from bucket
     * @return - ResponseEntity<Resource> - A stream of the S3Object returned as a resource.
     */
    public ResponseEntity<Resource> streamResource(S3Object dataObject) {
        try {
            //Extract InputStreamResource from S3 Object
            InputStream objectStream = dataObject.getObjectContent();
            InputStreamResource resourceStream = new InputStreamResource(objectStream);

            //Set necessary headers including file name
            ContentDisposition contentDisposition = ContentDisposition
                    .builder(CONTENT_DISPOSITION_INLINE)
                    .filename(dataObject.getKey())
                    .build();

            HttpHeaders headers = new HttpHeaders();
            headers.setContentDisposition(contentDisposition);

            //Return resource stream
            return ResponseEntity
                    .ok()
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    .headers(headers)
                    .body(resourceStream);
        } catch(Exception ex){
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR,
                    ex.getMessage(),
                    ex
            );
        }
    }

}
