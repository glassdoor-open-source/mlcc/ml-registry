pluginManagement {
    repositories {
        maven(url = extra["gd_maven_repo_url"] as String)
    }
}

rootProject.name = "ml-registry"
include("ml-registry-app")
include("ml-registry-core")
include("ml-registry-client")
include("ml-registry-api")
