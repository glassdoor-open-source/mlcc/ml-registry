# ML-Registry: Glassdoor's Centralized Artifact Management Solution

## Table of Contents

1. [Introduction](#introduction)
2. [Why ML-Registry?](#why-ml-registry)
3. [Unique Features](#unique-features)
4. [API Documentation](#api-documentation)
5. [Getting Started](#getting-started)
6. [Deployment](#deployment)
7. [AWS Credentials](#aws-credentials)
8. [Client Generation](#client-generation)

## Introduction

The ML-Registry, part of Glassdoor's ML platform, Glassmind, is designed as a centralized management hub for all
machine learning artifacts and metadata.

> **Engineering Blog**: [Introducing Glassdoor's ML Registry: A Centralized Artifact Management Solution](https://medium.com/glassdoor-engineering/introducing-glassdoors-ml-registry-a-centralized-artifact-management-solution-8bff3151cd9d)

## Why ML-Registry?

The ML-Registry addresses the crucial questions of the ML lifecycle:

- **Where are models and data stored?**
- **How to access and manage these assets?**
- **How to version and update them?**

It serves as a single source of truth for all data related to ML, providing uniform and reliable access across various 
teams and applications.

## Unique Features

### Git-Centric Architecture

Built with a Git-first philosophy, the ML-Registry relies on `artifacts.yml` configuration files stored in each 
repository and branch. These YAML files allow teams to define metadata specific to their projects, and this metadata is 
subsequently synchronized into a Redis cache. Read our [Engineering Blog](https://medium.com/glassdoor-engineering/introducing-glassdoors-ml-registry-a-centralized-artifact-management-solution-8bff3151cd9d) for a deep dive into our architecture.

### Flexibility

Beyond its core functionalities, the ML-Registry allows you to implement your own functionality by exposing interfaces 
that abstract all essential capabilities. This means you can customize the registry to your specific technology stack
and application needs.

## Technical Specifications

Glassdoor's ML-Registry utilizes the following technologies:

- **Java 17**
- **Gradle 7.4.X**: 
- **OAS3 Compliant**: The application API is fully compliant with the OpenAPI 3 Specification, offering standardized API 
documentation and client generation.
- **Redis Caching**: To provide fast access to metadata, Glassdoor uses Redis as its caching layer.
- **S3 Storage**: All data artifacts are securely stored in Amazon S3, ensuring durability and high availability.

## Getting Started

### Building the Project

To compile the project, execute the following:

```bash
./gradlew build --info
```

### Consumer Registration

1. **Add Repository**: List your repository in `~/repositories.yml` file, this lets the ML-Registry know you are a
consumer.
2. **CI/CD Setup**:
 - **Gitlab Users**: Adapt your `gitlab-ci.yml` to include refresh fucntionality from this project's `gitlab-ci.yml`. 
The idea is that the ML-Registry should always be refreshed after a Pull Request has been merged.
 - **Non-GitLab Users**: Add a step in your pipeline to call the /refresh API from the ML-Registry when a `variant-`
prefixed branch is merged.
3. **Refreshing**: The ML-Registry will update at regular intervals, learning of newly registered consumers. For an 
immediate refresh, use the Swagger endpoint.

## Deployment

Clone the project repository and modify the `application.yml` file with your keys, or inject them as suits your application.

## AWS Credentials

For local setups, make sure that you have AWS credentials correctly configured, as the `DefaultAWSCredentialsProviderChain` 
will automatically read these. For QA, Stage and Prod environments, make certain your application is configured with 
proper access the S3 buckets you are using.

## Client Generation

- **API Specs**: Download the latest OAS3 YAML from `<APP-URL>/api-docs.yaml`.
- **Update YAML**: Replace the content of `ml-registry-client/src/main/resources/ml-registry-app.yaml` with the downloaded specs.
- **Run Build**:

    ```bash
    ./gradlew clean build --info
    ```

- **Output**: The auto-generated client will be located at `ml-registry-client/build/generated`.

---

For further queries or contributions, please open an issue or submit a pull request.
